/*
 @student Daniel Park
 @pid A10229338
 @filename TestBank.java
 @lab# 2
 */

package lab2.test;

import lab2.bookstore.CreditCard;
import lab2.bank.IBank;

public class TestBank implements IBank {

	private boolean accept=false;
	@Override
	public boolean authorizePayment(CreditCard card, int price) {
		return accept;
	}

	public void setAccept(boolean b) {
		accept=b;
	}
	
	
}
