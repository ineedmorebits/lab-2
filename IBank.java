/*
 @student Daniel Park
 @pid A10229338
 @filename IBank.java
 @lab# 2
 */

package lab2.bank;

import lab2.bookstore.CreditCard;

public interface IBank {
	public boolean authorizePayment(CreditCard card, int price);
}

