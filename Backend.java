/*
 @student Daniel Park
 @pid A10229338
 @filename Backend.java
 @lab# 2
 */

package lab2.bookstore;

import java.util.HashSet;
import java.util.HashMap;

public class Backend implements IBackend{

  void addInformation(Product product, int price) throws BookstoreException
  {
    if (database.containsKey(product))
    {
      throw new BookstoreException("product already in the database");
    }
    ProductInformation p = new ProductInformation(product, price, 1);
    database.put(product, p);
    return;
  }

  void add(Product product) throws BookstoreException
  {
    if (database.containsKey(product))
       database.get(product).setNumberOfItemsAvailable
       (database.get(product).getNumberOfItemsAvailable() + 1);
    else 
       throw new BookstoreException("no such product in the database");
    return;
  }

  void remove(Product product)throws BookstoreException
  {
    if (!database.containsKey(product))
     throw new BookstoreException("no such product in the database");
    else if (database.get(product).getNumberOfItemsAvailable() < 1)
     throw new BookstoreException("no more such products available");
     else
      database.get(product).setNumberOfItemsAvailable
      (database.get(product).getNumberOfItemsAvailable() - 1);
    return;
  }

  ProductInformation lookup(Product product)
  {
    if (database.containsKey(product))
      return database.get(product);
    else
      return null;
  }
}
