/*
 @student Daniel Park
 @pid A10229338
 @filename Frontend.java
 @lab# 2
 */

public class Frontend implements IFrontend
{
  void addToCart(Product product, Cart cart)
  {
     cart.addItem(product);
  }

  void removeFromCart(Product product, Cart cart)
  {
     cart.removeItem(product);
  }

  Set<ProductInformation> searchProduct (String nameOfProduct)
  {
      HashSet<ProductInformation> p = new HashSet<ProductInformation>();
      Iterator<Product> t = database.keySet().iterator();
      for (; t.hasNext(); t = t.next())
      {
        if (t.next().getName() == nameOfProduct)
        {
           p.add(t);
        }
      }
      return p;
  }

  int totalPrice (Cart cart)
  {
     return cart.getTotalPrice();
  }

  List<Product> getCartContent(Cart cart)
  {
    List<Product> cartContent = new List<Product>();
      for (int i = 0; i < cart.getContent().length; i++)
      {
         cartContent.add((cart.getContent()[i]);
      }
    return cartContent;
  }

  void checkout (String name,String addressOfDelivery, 
              CreditCard card, Cart cart) throws BookstoreException
  {

  }
}
