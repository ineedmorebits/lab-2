/*
 @student Daniel Park
 @pid A10229338
 @filename Bank.java
 @lab# 2
 */

package lab2.bank;

import lab2.bookstore.CreditCard;

public class Bank implements IBank {

	@Override
	public boolean authorizePayment(CreditCard card, int price){
		
		if(Math.random()>0.5)
			return true;
		else
			return false;
	}
}
