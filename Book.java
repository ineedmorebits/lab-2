/*
 @student Daniel Park
 @pid A10229338
 @filename Book.java
 @lab# 2
 */

package lab2.products;
public class Book extends Product{
  private String name;
  private String description;
  private int pageNumber;

  public Book(String name,String description,
             int pageNumber) 
  {
     this.name = name;
     this.description = description;
     this.pageNumber = pageNumber;    
  }
  public String getName()
  {
     return this.name;
  }
  public String getDescription()
  {
     return this.description;
  }
  public int getPageNumber()
  {
     return this.pageNumber;
  }
  @Override
  public boolean equals(Book book)
  {
     if (book == null)
      return false;

     return (this.name == book.getName()
          && this.description == book.getDescription()
          && this.pageNumber == book.getPageNumber())
  }
  @Override
  public int hashCode()
  {
     int hash = 1;
     hash = hash * 17 + this.pageNumber;
     hash = hash * 31 + (this.name == null?0:this.name.hashCode());
     hash = hash * 31 + (this.description == null?0
                         :this.description.hashCode());
     return hash;
  }
}
