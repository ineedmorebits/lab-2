/*
 @student Daniel Park
 @pid A10229338
 @filename CreditCard.java
 @lab# 2
 */

package lab2.bookstore;

public class CreditCard {

	public static final int VISA = 0;
	public static final int MASTERCARD = 1;
	public static final int AMERICANEXPRESS = 2;
	
	private String name;
	private String number;
	private String expiration;
	private String securityCode;
	private int type;
	public CreditCard(String name, String number, String expiration,
			String securityCode, int type) throws BookstoreException{
		super();
		this.name = name;
		this.number = number;
		this.expiration = expiration;
		this.securityCode = securityCode;
		if (type != VISA & type!=MASTERCARD & type!=AMERICANEXPRESS) 
			throw new BookstoreException("Invalid Credit Card Type");
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public String getNumber() {
		return number;
	}
	public String getExpiration() {
		return expiration;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public String getType() throws BookstoreException{
		if (type==VISA) return "VISA";
		if(type==MASTERCARD)return "MASTERCARD";
		if(type==AMERICANEXPRESS)return "AMERICANEXPRESS";
		
		throw new BookstoreException();
	}	
}

