/*
 @student Daniel Park
 @pid A10229338
 @filename IFrontend.java
 @lab# 2
 */

package lab2.bookstore;

import java.util.List;
import java.util.Set;

import lab2.products.Product;


public interface IFrontend {
	
	void addToCart(Product product, Cart cart);
	void removeFromCart(Product product, Cart cart);
	Set<ProductInformation> searchProduct (String nameOfProduct);
	void checkout (String name, String addressOfDelivery, CreditCard card, Cart cart) throws BookstoreException;
	int totalPrice (Cart cart);
	List<Product> getCartContent(Cart cart);

}
