/*
 @student Daniel Park
 @pid A10229338
 @filename TestBookstore.java
 @lab# 2
 */

package lab2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Set;

import junit.framework.Assert;
import lab2.bookstore.Cart;
import lab2.bookstore.CreditCard;
import lab2.bookstore.Store;
import lab2.products.Product;
import lab2.bookstore.BookstoreException;

import org.junit.Before;
import org.junit.Test;



public class TestBookstore {
	
	Store store;
	TestBank testBank;
	Backend backend;
	Frontend frontend;
	Product B1,D1,M1;
	
	@Before
	public void initializeDatabase() {
			
		testBank = new TestBank();
		store = new Store(testBank);
		backend  = store.getBackend();
		frontend = store.getFrontend();
		B1 = new Book("Alice in Wonderland", "Cool book", 100);
		try {
			backend.addInformation(B1, 30);	
		} catch (BookstoreException e) {
			fail(e.getMessage());
		}
		

		D1 = new DVD("Alice in Wonderland", "Cool movie", 120);
		try {
			backend.addInformation(D1, 18);	
		} catch (BookstoreException e) {
			fail(e.getMessage());
		}
		
		M1 = new Magazine("Wonderland Times", "Best News", 40);
		try {
			backend.addInformation(M1, 9);	
		} catch (BookstoreException e) {
			fail(e.getMessage());
		}
		
	}
	
	@Test
	public void testAddProducts() throws BookstoreException {
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		
		backend.add(M1);
		backend.add(M1);
		
		assertEquals(4, backend.lookup(B1).getNumberOfItemsAvailable());
		assertEquals(0, backend.lookup(D1).getNumberOfItemsAvailable());
		assertEquals(2, backend.lookup(M1).getNumberOfItemsAvailable());
		
	}

	@Test
	public void testRemoveProducts() throws BookstoreException {
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		
		backend.add(M1);
		backend.add(M1);
		

		backend.remove(B1);
		backend.remove(M1);
		
		assertEquals(3, backend.lookup(B1).getNumberOfItemsAvailable());
		assertEquals(0, backend.lookup(D1).getNumberOfItemsAvailable());
		assertEquals(1, backend.lookup(M1).getNumberOfItemsAvailable());
		
		try {
			backend.remove(D1);
		} catch(BookstoreException e) {
			return;
		}
		fail();
	}

	@Test
	public void testSearchProduct() throws BookstoreException {
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		
		backend.add(M1);
		backend.add(M1);
		
		Set<ProductInformation> pset = frontend.searchProduct("Alice in Wonderland");
		
		assertEquals(2,pset.size());
		
		for(ProductInformation pi : pset) {
			assertTrue(pi.getProduct().equals(B1) | pi.getProduct().equals(D1));
		}
		
	}
	
	@Test
	public void testAddAndRemoveItemsToCart() throws BookstoreException {

		Cart cart = new Cart();
		
		frontend.addToCart(B1, cart);
		frontend.addToCart(D1, cart);
		frontend.addToCart(M1, cart);
		

		List<Product> cartProducts = frontend.getCartContent(cart);
		
		assertTrue(cartProducts.contains(B1));
		assertTrue(cartProducts.contains(M1));
		assertTrue(cartProducts.contains(D1));
		
		frontend.removeFromCart(D1, cart);
		cartProducts = frontend.getCartContent(cart);
		assertFalse(cartProducts.contains(D1));
	}
	
	@Test
	public void testPriceItemsInCart() throws BookstoreException {

		Cart cart = new Cart();
		
		frontend.addToCart(B1, cart);
		frontend.addToCart(D1, cart);
		frontend.addToCart(M1, cart);
		frontend.addToCart(B1, cart);
		frontend.addToCart(B1, cart);
		frontend.addToCart(M1, cart);
		
		frontend.removeFromCart(D1, cart);
		frontend.removeFromCart(B1, cart);

		assertEquals(78, frontend.totalPrice(cart));
	}
	
	@Test
	public void testCheckoutAvailability() throws BookstoreException {
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		
		backend.add(M1);
		backend.add(M1);
		
		
		Cart cart = new Cart();
		
		frontend.addToCart(B1, cart);
		frontend.addToCart(M1, cart);
		frontend.addToCart(D1, cart);
		
		try {
			frontend.checkout("Test", "Home", new CreditCard("Test", "0000", "12/2020", "1234", CreditCard.VISA), cart);
		} catch(BookstoreException e) {
			return;
		}
		fail();		
	}

	@Test
	public void testCheckoutBank() throws BookstoreException {
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		
		backend.add(M1);
		backend.add(M1);
		
		
		Cart cart = new Cart();
		
		frontend.addToCart(B1, cart);
		frontend.addToCart(M1, cart);
		
		testBank.setAccept(false);
		
		try {
			frontend.checkout("Test", "Home", new CreditCard("Test", "0000", "12/2020", "1234", CreditCard.VISA), cart);
		} catch(BookstoreException e) {
			assertEquals(4,backend.lookup(B1).getNumberOfItemsAvailable());
			assertEquals(2,backend.lookup(M1).getNumberOfItemsAvailable());
			assertEquals(0,backend.lookup(D1).getNumberOfItemsAvailable());
			return;
		}
		fail();		
	}

	@Test
	public void testCheckoutItems() throws BookstoreException {
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		
		backend.add(M1);
		backend.add(M1);
		
		
		Cart cart = new Cart();
		
		frontend.addToCart(B1, cart);
		frontend.addToCart(M1, cart);
		
		testBank.setAccept(true);
		
		try {
			frontend.checkout("Test", "Home", new CreditCard("Test", "0000", "12/2020", "1234", CreditCard.VISA), cart);
		} catch(BookstoreException e) {
			fail();		
		}

		assertEquals(3,backend.lookup(B1).getNumberOfItemsAvailable());
		assertEquals(1,backend.lookup(M1).getNumberOfItemsAvailable());
		assertEquals(0,backend.lookup(D1).getNumberOfItemsAvailable());
	}
	
	@Test
	public void testSafetyDataStructures() throws BookstoreException {
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		backend.add(B1);
		
		backend.add(M1);
		backend.add(M1);
		
		backend.lookup(B1).setNumberOfItemsAvailable(100);
		
		assertEquals(4, backend.lookup(B1).getNumberOfItemsAvailable());
	}
	
	@Test
	public void testProductAndProductInformation() throws BookstoreException {
		
		Product P1 = new Book("Alice in Wonderland", "Cool book", 100);
		Product P2 = new Book("Alice in Wonderland", "Cool book", 100);
		
		assertEquals(P1, P2);
		assertEquals(P1.hashCode(), P2.hashCode());
		
		ProductInformation I1 = new ProductInformation(P1, 100, 35);
		ProductInformation I2 = new ProductInformation(P2, 100, 35);
		
		assertEquals(I1, I2);
		assertEquals(I1.hashCode(), I2.hashCode());
	
	}
}
