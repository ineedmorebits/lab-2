/*
 @student Daniel Park
 @pid A10229338
 @filename ProductInformation.java
 @lab# 2
 */

package lab2.bookstore;

public class ProductInformation {
  private Product product;
  private int price;
  private int numberOfItemsAvailable;
  public ProductInformation(Product product, int price,
                          int numberOfItemsAvailable) 
  {
    this.product = product;
    this.price = price;
    this.numberOfItemsAvailable = numberOfItemsAvailable;
  }
  public int getNumberOfItemsAvailable() 
  {
    return numberOfItemsAvailable;
  }
  public void setNumberOfItemsAvailable(int numberOfItemsAvailable) 
  {
     this.numberOfItemsAvailable = numberOfItemsAvailable;
     return;
  }
  public int getPrice()
  {
     return this.price;
  }
}

