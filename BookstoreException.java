/*
 @student Daniel Park
 @pid A10229338
 @filename BookstoreException.java
 @lab# 2
 */

package lab2.bookstore;

public class BookstoreException extends Exception {

	private static final long serialVersionUID = 1L;

	public BookstoreException() {
		super();
	}

	public BookstoreException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public BookstoreException(String arg0) {
		super(arg0);
	}

	public BookstoreException(Throwable arg0) {
		super(arg0);
	}

}
