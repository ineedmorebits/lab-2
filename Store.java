/*
 @student Daniel Park
 @pid A10229338
 @filename Store.java
 @lab# 2
 */

package lab2.bookstore;

import java.util.HashMap;

import lab2.products.Product;

import lab2.bank.IBank;



public class Store  {

	protected HashMap<Product, ProductInformation> database;
	protected IBank bank;
	protected Frontend frontend;
	protected Backend backend;
	
	public Store(IBank bank){
		database= new HashMap<Product, ProductInformation>();
		this.bank= bank;
		frontend = new Frontend(this);
		backend = new Backend(this);
		Cart.setBackend(backend);
	}

	public Frontend getFrontend() {
		return frontend;
	}

	public Backend getBackend() {
		return backend;
	}

}
