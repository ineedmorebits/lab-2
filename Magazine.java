/*
 @student Daniel Park
 @pid A10229338
 @filename Magazine.java
 @lab# 2
 */

package lab2.products;
public class Magazine extends Product {
  private String name;
  private String description;
  private int weekNumber;
  public Magazine(String name, String description,
                int weekNumber) 
  {
    this.name = name;
    this.description = description;
    this.weekNumber = weekNumber;
  }
  public int getWeekNumber()
  {
    return this.weekNumber;
  }
  public String getDescription()
  {
    return this.description;
  }
  public String getName()
  {
    return this.name;
  }
  @Override
  public boolean equals(Magazine magazine)
  {
     if (magazine == null)
      return false;

     return (this.name == magazine.getName()
          && this.description == magazine.getDescription()
          && this.weekNumber == magazine.getWeekNumber())
  }
  @Override
  public int hashCode()
  {
     int hash = 1;
     hash = hash * 17 + this.weekNumber;
     hash = hash * 31 + (this.name == null? 0: this.name.hashCode());
     hash = hash * 31 + (this.description == null? 0: this.description.hashCode());
     return hash;
  }
}
