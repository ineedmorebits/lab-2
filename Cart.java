/*
 @student Daniel Park
 @pid A10229338
 @filename Cart.java
 @lab# 2
 */

package lab2.bookstore;

import java.util.LinkedList;
import java.util.List;

import lab2.products.Product;


public class Cart {

	private List<Product> content;
	private static IBackend backend;
	
	public static void setBackend(IBackend backend) {
		Cart.backend = backend;
	}

	public Cart() {
		content= new LinkedList<Product>();
	}
	
	public int getTotalPrice(){
		int price=0;
		for (Product p : content){
			price+=backend.lookup(p).getPrice();
		}
		return price;
	}
	
	public void addItem(Product product){
		content.add(product);
	}
	
	public void removeItem(Product product){
		content.remove(product);
	}
	
	public Product[] getContent(){
		return content.toArray(new Product[0]);
	}
}

