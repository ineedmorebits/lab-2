/*
 @student Daniel Park
 @pid A10229338
 @filename DVD.java
 @lab# 2
 */

package lab2.products;
public class DVD extends Product {
  private String name;
  private String description;
  private int duration;

  public DVD(String name,String description,
            int duration)
  {
    this.name = name;
    this.description = description;
    this.duration = duration;
  } 
  public String getName()
  {
    return this.name;
  }  
  public String getDescription()
  {
    return this.description()
  }
  public int getDuration()
  {
     return this.duration;
  }
  @Override
  public boolean equals(DVD dvd)
  {
     if (dvd == null)
     return false;

     return (this.name == dvd.getName()
          && this.description == dvd.getDescription()
          && this.duration == dvd.getDuration())
  }
  @Override
  public int hashCode()
  {
     int hash = 1;
     hash = hash * 17 + this.duration;
     hash = hash * 31 + (this.name == null?0: this.name.hashCode());
     hash = hash * 31 + (this.description == null?0
                          :this.description.hashCode());
     return hash;
  }
}
